﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIScript : MonoBehaviour {

	public GameObject GameManager;
	public GameObject TableManager;
	public Sprite suns, spades, hearts, diamonds, clubs;
	public int bidAmount;
	public int bidColor;
	public int callAmount;
	// Use this for initialization
	void Start () {
		GameManager = GameObject.Find("GameManager");
		TableManager = GameObject.Find("TableManager");
	}
	void callForNewTable () {
	}
	// Update is called once per frame
	void Update () {
		//UpdateBid();
		//MakeBid();
		UIUpdate();
	}

	public void UIUpdate() {
	
		if(gameObject.name == "Player1Name")
		{
			gameObject.GetComponent<Text>().fontSize = 32;
			gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().player1Name;
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 1)
			{
				gameObject.GetComponent<Text>().fontSize = 42;
			}
		}
		else if(gameObject.name == "Player2Name")
		{
			gameObject.GetComponent<Text>().fontSize = 32;
			gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().player2Name;
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 2)
			{
				gameObject.GetComponent<Text>().fontSize = 42;
			}
		}
		else if(gameObject.name == "Player3Name")
		{
			gameObject.GetComponent<Text>().fontSize = 32;
			gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().player3Name;
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 3)
			{
				gameObject.GetComponent<Text>().fontSize = 42;
			}
		}
		else if(gameObject.name == "Player4Name")
		{
			gameObject.GetComponent<Text>().fontSize = 32;
			gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().player4Name;
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 4)
			{
				gameObject.GetComponent<Text>().fontSize = 42;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if(gameObject.name == "BidAmountButton")
		{
			if(TableManager.GetComponent<TableManagerScript>().highestBidAmount > bidAmount || (TableManager.GetComponent<TableManagerScript>().highestBidAmount == bidAmount && TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor <= TableManager.GetComponent<TableManagerScript>().highestBidColor) || bidAmount <= 3)
			{
				GetComponent<Button>().interactable = false;
			}
			else if(!GetComponent<Button>().IsInteractable())
			{
				GetComponent<Button>().interactable = true;
			}
		}
		if(gameObject.name == "BidColorButton")
		{
			if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount == TableManager.GetComponent<TableManagerScript>().highestBidAmount && TableManager.GetComponent<TableManagerScript>().highestBidColor > bidColor)
			{
				GetComponent<Button>().interactable = false;
			}
			else if(!GetComponent<Button>().IsInteractable())
			{
				GetComponent<Button>().interactable = true;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	CallAmountButton checks the state of the table, by knowing if it's still the first caller (winning bidder) 
		//	making their call in that case it restricts options to calls higher than winning bid, else treat as a normal caller
		
		//	Normal caller cannot have their call increase total calls to 13
		//	also checks if the call option is a With option, Risk option, Double Risk option
		
		if(gameObject.name == "CallAmountButton")
		{
			gameObject.GetComponentInChildren<Text>().text = callAmount.ToString();
			if(TableManager.GetComponent<TableManagerScript>().remainingCalls == 4)
			{
				if(callAmount < TableManager.GetComponent<TableManagerScript>().highestBidAmount)
				{
					GetComponent<Button>().interactable = false;
				}
				else {
					GetComponent<Button>().interactable = true;
				}
			}
			else 
			{
				if( (TableManager.GetComponent<TableManagerScript>().totalEstimationCalls+callAmount == 13) || (callAmount > TableManager.GetComponent<TableManagerScript>().highestBidAmount) )
				{
					GetComponent<Button>().interactable = false;
				}
				else
				{
					if(callAmount == TableManager.GetComponent<TableManagerScript>().highestBidAmount)
					{
						gameObject.GetComponentInChildren<Text>().text = callAmount.ToString()+"W";
						if((TableManager.GetComponent<TableManagerScript>().remainingCalls == 1 || (TableManager.GetComponent<TableManagerScript>().remainingCalls == 2 && TableManager.GetComponent<TableManagerScript>().thereIsADashCaller)))
						{
							if(TableManager.GetComponent<TableManagerScript>().totalEstimationCalls+callAmount > 10 && TableManager.GetComponent<TableManagerScript>().totalEstimationCalls+callAmount < 16)
							{
								gameObject.GetComponentInChildren<Text>().text = callAmount.ToString()+"W-R";
							}
						}
					}
					else if((TableManager.GetComponent<TableManagerScript>().remainingCalls == 1 || (TableManager.GetComponent<TableManagerScript>().remainingCalls == 2 && TableManager.GetComponent<TableManagerScript>().thereIsADashCaller)))
					{
						if(TableManager.GetComponent<TableManagerScript>().totalEstimationCalls+callAmount > 10 && TableManager.GetComponent<TableManagerScript>().totalEstimationCalls+callAmount < 16)
						{
							gameObject.GetComponentInChildren<Text>().text = callAmount.ToString()+"R";
						}
					}
					else if(gameObject.GetComponentInChildren<Text>().text != callAmount.ToString())
					{
						gameObject.GetComponentInChildren<Text>().text = callAmount.ToString();
					}
					GetComponent<Button>().interactable = true;
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		else if(gameObject.name == "WinningBidIndicator")
		{
			if(TableManager.GetComponent<TableManagerScript>().highestBidAmount > 0)
			{
				gameObject.GetComponentInChildren<Text>().text = TableManager.GetComponent<TableManagerScript>().highestBidAmount.ToString();
			}
			if(TableManager.GetComponent<TableManagerScript>().highestBidColor == 0)
			{
				gameObject.GetComponent<Image>().color = Color.white;
				gameObject.GetComponent<Image>().sprite = clubs;
			}
			else if(TableManager.GetComponent<TableManagerScript>().highestBidColor == 1)
			{
				gameObject.GetComponent<Image>().color = Color.white;
				gameObject.GetComponent<Image>().sprite = diamonds;
			}
			else if(TableManager.GetComponent<TableManagerScript>().highestBidColor == 2)
			{
				gameObject.GetComponent<Image>().color = Color.white;
				gameObject.GetComponent<Image>().sprite = hearts;
			}
			else if(TableManager.GetComponent<TableManagerScript>().highestBidColor == 3)
			{
				gameObject.GetComponent<Image>().color = Color.white;
				gameObject.GetComponent<Image>().sprite = spades;
			}
			else 
			{
				gameObject.GetComponent<Image>().color = Color.clear;
				gameObject.GetComponent<Image>().sprite = null;
				gameObject.GetComponentInChildren<Text>().text = "";
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		if(gameObject.name == "Player1BidCount")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 1)
			{
				gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount.ToString();
			}
			if(GameObject.Find(TableManager.GetComponent<TableManagerScript>().player1Name).GetComponent<PlayerScript_ECalculator>().dashCall)
			{
				gameObject.GetComponent<Text>().text = "Dash";
			}
		}
		else if(gameObject.name == "Player2BidCount")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 2)
			{
				gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount.ToString();
			}
			if(GameObject.Find(TableManager.GetComponent<TableManagerScript>().player2Name).GetComponent<PlayerScript_ECalculator>().dashCall)
			{
				gameObject.GetComponent<Text>().text = "Dash";
			}
		}
		else if(gameObject.name == "Player3BidCount")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 3)
			{
				gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount.ToString();
			}
			if(GameObject.Find(TableManager.GetComponent<TableManagerScript>().player3Name).GetComponent<PlayerScript_ECalculator>().dashCall)
			{
				gameObject.GetComponent<Text>().text = "Dash";
			}
		}
		else if(gameObject.name == "Player4BidCount")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 4)
			{
				gameObject.GetComponent<Text>().text = TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount.ToString();
			}
			if(GameObject.Find(TableManager.GetComponent<TableManagerScript>().player4Name).GetComponent<PlayerScript_ECalculator>().dashCall)
			{
				gameObject.GetComponent<Text>().text = "Dash";
			}
		}
	
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		else if(gameObject.name == "Player1BidColor")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 1)
			{
				if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 0)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = clubs;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 1)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = diamonds;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 2)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = hearts;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 3)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = spades;
				}
				else
				{
					gameObject.GetComponent<Image>().color = Color.clear;
					gameObject.GetComponent<Image>().sprite = null;
				}
			}
		}
		else if(gameObject.name == "Player2BidColor")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 2)
			{
				if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 0)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = clubs;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 1)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = diamonds;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 2)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = hearts;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 3)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = spades;
				}
				else
				{
					gameObject.GetComponent<Image>().color = Color.clear;
					gameObject.GetComponent<Image>().sprite = null;
				}
			}
		}
		else if(gameObject.name == "Player3BidColor")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 3)
			{
				if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 0)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = clubs;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 1)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = diamonds;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 2)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = hearts;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 3)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = spades;
				}
				else
				{
					gameObject.GetComponent<Image>().color = Color.clear;
					gameObject.GetComponent<Image>().sprite = null;
				}
			}
		}
		else if(gameObject.name == "Player4BidColor")
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 4)
			{
				if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 0)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = clubs;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 1)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = diamonds;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 2)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = hearts;
				}
				else if(TableManager.GetComponent<TableManagerScript>().ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor == 3)
				{
					gameObject.GetComponent<Image>().color = Color.white;
					gameObject.GetComponent<Image>().sprite = spades;
				}
				else
				{
					gameObject.GetComponent<Image>().color = Color.clear;
					gameObject.GetComponent<Image>().sprite = null;
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else if(gameObject.name == "Player1CallCount")
		{
			gameObject.GetComponent<Text>().text = GameObject.Find(TableManager.GetComponent<TableManagerScript>().player1Name).GetComponent<PlayerScript_ECalculator>().callCount.ToString();
		}
		else if(gameObject.name == "Player2CallCount")
		{
			gameObject.GetComponent<Text>().text = GameObject.Find(TableManager.GetComponent<TableManagerScript>().player2Name).GetComponent<PlayerScript_ECalculator>().callCount.ToString();
		}
		else if(gameObject.name == "Player3CallCount")
		{
			gameObject.GetComponent<Text>().text = GameObject.Find(TableManager.GetComponent<TableManagerScript>().player3Name).GetComponent<PlayerScript_ECalculator>().callCount.ToString();
		}
		else if(gameObject.name == "Player4CallCount")
		{
			gameObject.GetComponent<Text>().text = GameObject.Find(TableManager.GetComponent<TableManagerScript>().player4Name).GetComponent<PlayerScript_ECalculator>().callCount.ToString();
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else if(gameObject.name == "EndRoundButton")
		{
			int player1Tricks = int.Parse(GameObject.Find("Player1Tricks").GetComponent<InputField>().text);
			int player2Tricks = int.Parse(GameObject.Find("Player2Tricks").GetComponent<InputField>().text);
			int player3Tricks = int.Parse(GameObject.Find("Player3Tricks").GetComponent<InputField>().text);
			int player4Tricks = int.Parse(GameObject.Find("Player4Tricks").GetComponent<InputField>().text);
			if(player1Tricks+player2Tricks+player3Tricks+player4Tricks == 13)
			{
				GetComponent<Button>().interactable = true;
			}
			else
			{
				GetComponent<Button>().interactable = false;
			}
		}
	}


	public void UpdateBid() {
		if(TableManager)
		{
			if(gameObject.name == "BidAmountButton")
			{
				TableManager.GetComponent<TableManagerScript>().UpdateBidAmount(bidAmount);
			}
			if(gameObject.name == "BidColorButton")
			{
				TableManager.GetComponent<TableManagerScript>().UpdateBidColor(bidColor);
			}
		}
	}
	public void MakeBid() {
		if(TableManager)
		{
			TableManager.GetComponent<TableManagerScript>().MakeBid();
		}
	}
	public void PassBid() {
		if(TableManager)
		{
			if(TableManager.GetComponent<TableManagerScript>().playerTurn == 1)
			{
				GameObject.Find("Player1BidCount").GetComponent<Text>().text = "Pass";
				GameObject.Find("Player1BidColor").GetComponent<Image>().sprite = null;
				GameObject.Find("Player1BidColor").GetComponent<Image>().color = Color.clear;
			}
			else if(TableManager.GetComponent<TableManagerScript>().playerTurn == 2)
			{
				GameObject.Find("Player2BidCount").GetComponent<Text>().text = "Pass";
				GameObject.Find("Player2BidColor").GetComponent<Image>().sprite = null;
				GameObject.Find("Player2BidColor").GetComponent<Image>().color = Color.clear;
			}
			else if(TableManager.GetComponent<TableManagerScript>().playerTurn == 3)
			{
				GameObject.Find("Player3BidCount").GetComponent<Text>().text = "Pass";
				GameObject.Find("Player3BidColor").GetComponent<Image>().sprite = null;
				GameObject.Find("Player3BidColor").GetComponent<Image>().color = Color.clear;
			}
			else if(TableManager.GetComponent<TableManagerScript>().playerTurn == 4)
			{
				GameObject.Find("Player4BidCount").GetComponent<Text>().text = "Pass";
				GameObject.Find("Player4BidColor").GetComponent<Image>().sprite = null;
				GameObject.Find("Player4BidColor").GetComponent<Image>().color = Color.clear;
			}
			TableManager.GetComponent<TableManagerScript>().PassBid();
		}
	}
	public void BeginBidding() {
		TableManager.GetComponent<TableManagerScript>().BeginBidding();
		
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void BeginCalling() {
		TableManager.GetComponent<TableManagerScript>().BeginCalls();
	}
	public void UpdateCall() {
		if(TableManager)
		{
			if(gameObject.name == "CallAmountButton")
			{
				TableManager.GetComponent<TableManagerScript>().UpdateCallAmount(callAmount);
			}
		}
	}
	public void MakeCall() {
		if(TableManager)
		{
			TableManager.GetComponent<TableManagerScript>().MakeCall();
		}
	}
	public void UpdateTableStats() {
		Debug.Log(GameObject.Find("Canvas/TableStatsPanel/Player1StatLabels/Round ("+1+")/Calls"));
		for (int i = 1; i <= 18; i++)
		{
			GameObject.Find("Canvas/TableStatsPanel/Player1StatLabels/Round ("+i+")/Calls").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[0].call.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player1StatLabels/Round ("+i+")/Tricks").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[0].tricks.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player1StatLabels/Round ("+i+")/Score").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[0].score.ToString();
			/////////////////////
			GameObject.Find("Canvas/TableStatsPanel/Player2StatLabels/Round ("+i+")/Calls").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[1].call.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player2StatLabels/Round ("+i+")/Tricks").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[1].tricks.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player2StatLabels/Round ("+i+")/Score").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[1].score.ToString();
			/////////////////////
			GameObject.Find("Canvas/TableStatsPanel/Player3StatLabels/Round ("+i+")/Calls").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[2].call.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player3StatLabels/Round ("+i+")/Tricks").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[2].tricks.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player3StatLabels/Round ("+i+")/Score").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[2].score.ToString();
			/////////////////////
			GameObject.Find("Canvas/TableStatsPanel/Player4StatLabels/Round ("+i+")/Calls").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[3].call.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player4StatLabels/Round ("+i+")/Tricks").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[3].tricks.ToString();
			GameObject.Find("Canvas/TableStatsPanel/Player4StatLabels/Round ("+i+")/Score").GetComponent<Text>().text = TableManager.GetComponent<RoundStorage>().rounds[i-1].players[3].score.ToString();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void EndRound() {
		int player1Tricks = int.Parse(GameObject.Find("Player1Tricks").GetComponent<InputField>().text);
		int player2Tricks = int.Parse(GameObject.Find("Player2Tricks").GetComponent<InputField>().text);
		int player3Tricks = int.Parse(GameObject.Find("Player3Tricks").GetComponent<InputField>().text);
		int player4Tricks = int.Parse(GameObject.Find("Player4Tricks").GetComponent<InputField>().text);
		TableManager.GetComponent<RoundStorage>().EndRound(player1Tricks, player2Tricks, player3Tricks, player4Tricks);
	}
}
