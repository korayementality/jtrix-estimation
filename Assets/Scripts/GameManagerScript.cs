﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

	public string Player1Name, Player2Name, Player3Name, Player4Name;
	public bool newTable;

	// Use this for initialization
	void Start () {

		DontDestroyOnLoad(gameObject);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadScene(string targetScene)
	{
		SceneManager.LoadScene(targetScene, LoadSceneMode.Single);
		if(targetScene == "estimationtable")
		{
			CallForNewTable();
		}
	}
	public void CallForNewTable() {
		newTable = true;
	}
}
