﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableManagerScript : MonoBehaviour {
	public string player1Name, player2Name, player3Name, player4Name;
	public int playerTurn, highestBidAmount, highestBidColor, numberOfPassingPlayers, totalEstimationCalls, remainingCalls;
	public bool newTurn, bidding, estimating, playing, thereIsADashCaller;
	public GameObject ActivePlayer, WinningBidder;
	public GameObject GameManager, MenuManager;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
		GameManager = GameObject.Find("GameManager");
		BeginDashCalls();
		newTurn = true;
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void RegisterNames() {
		player1Name = GameObject.Find("PlayerOneNameField").GetComponent<InputField>().text;
		player2Name = GameObject.Find("PlayerTwoNameField").GetComponent<InputField>().text;
		player3Name = GameObject.Find("PlayerThreeNameField").GetComponent<InputField>().text;
		player4Name = GameObject.Find("PlayerFourNameField").GetComponent<InputField>().text;
		if(player1Name != "" && player2Name != "" && player3Name != "" && player4Name != "")
		{
			GameObject.Find("GameManager").GetComponent<GameManagerScript>().LoadScene("estimationcalculatortable");
			GetComponent<RoundStorage>().InitializeRoundStorage();
		}
		
	}
	public void BeginDashCalls() {
		numberOfPassingPlayers = 0;
		totalEstimationCalls = 0;
		playerTurn = -1;
		highestBidAmount = -1;
		highestBidColor = -1;
		WinningBidder = null;
		foreach (var player in GameObject.FindGameObjectsWithTag("ECalc_Player"))
		{
			player.GetComponent<PlayerScript_ECalculator>().callCount = 0;
			player.GetComponent<PlayerScript_ECalculator>().bidCount = 0;
			player.GetComponent<PlayerScript_ECalculator>().bidColor = 0;
			player.GetComponent<PlayerScript_ECalculator>().risk = false;
			player.GetComponent<PlayerScript_ECalculator>().passed = false;
			player.GetComponent<PlayerScript_ECalculator>().dashCall = false;
			player.GetComponent<PlayerScript_ECalculator>().win = false;
			player.GetComponent<PlayerScript_ECalculator>().with = false;
			player.GetComponent<PlayerScript_ECalculator>().doubleRisk = false;
			player.GetComponent<PlayerScript_ECalculator>().BidWinner = false;	
		}
	}
	public void UpdateTableStats() {
		totalEstimationCalls = 0;
		GameObject[] players = GameObject.FindGameObjectsWithTag("ECalc_Player");
		foreach(GameObject player in players)
		{
			totalEstimationCalls += player.GetComponent<PlayerScript_ECalculator>().callCount;
		}
	}
	
	public void nextTurn() {
		playerTurn++;
		//Check if bidding should end
		if(bidding)
		{
			if(numberOfPassingPlayers == 3 || (numberOfPassingPlayers == 2 && thereIsADashCaller) )
			{
				BeginCalls();
				GameObject.Find("MenuManager").GetComponent<MenuManagerScript>().SetMenuState(2);
			}
		}
		//Check if estimation should end
		if(estimating)
		{
			if(remainingCalls == 0 || (remainingCalls == 1 && thereIsADashCaller))
			{
				BeginPlay();
				GameObject.Find("MenuManager").GetComponent<MenuManagerScript>().SetMenuState(3);
			}	
		}
		playerTurn = playerTurn%5;
		if(playerTurn == 0)
		{
			playerTurn++;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void BeginBidding() {
		playerTurn = 1;
		bidding = true;
		playing = false;
	}
	public void MakeBid() {
		highestBidAmount = ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount;
		highestBidColor = ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor;
		WinningBidder = ActivePlayer;
		newTurn = true;
		nextTurn();
	}
	
	public void UpdateBidAmount(int amount) {
		ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount = amount;
	}
	
	public void UpdateBidColor(int color) {
		ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor = color;
	}

	public void PassBid() {
		ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidColor = -1;
		ActivePlayer.GetComponent<PlayerScript_ECalculator>().bidCount = 0;
		ActivePlayer.GetComponent<PlayerScript_ECalculator>().passed = true;
		numberOfPassingPlayers++;
		newTurn = true;
		nextTurn();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void BeginCalls() {
		remainingCalls = 4;
		bidding = false;
		estimating = true;
		GameObject[] players = GameObject.FindGameObjectsWithTag("ECalc_Player");
		foreach(GameObject player in players)
		{
			if(!player.GetComponent<PlayerScript_ECalculator>().dashCall)
			{
				player.GetComponent<PlayerScript_ECalculator>().passed = false;
			}
		}
		WinningBidder.GetComponent<PlayerScript_ECalculator>().callCount = highestBidAmount;
		WinningBidder.GetComponent<PlayerScript_ECalculator>().BidWinner = true;
		//WinningBidder.GetComponent<PlayerScript_ECalculator>().passed = true;
		playerTurn = WinningBidder.GetComponent<PlayerScript_ECalculator>().playerNumber;
		//totalEstimationCalls += highestBidAmount;
		//nextTurn();
	}
	public void UpdateCallAmount(int amount) {
		ActivePlayer.GetComponent<PlayerScript_ECalculator>().callCount = amount;
	}
	public void MakeCall() {
		totalEstimationCalls += ActivePlayer.GetComponent<PlayerScript_ECalculator>().callCount;
		// check if this is the last player making their call to mark them as Risk
		if((remainingCalls == 2 && thereIsADashCaller) || remainingCalls == 1)
		{
			if(totalEstimationCalls < 16 && totalEstimationCalls > 10)
			{
				ActivePlayer.GetComponent<PlayerScript_ECalculator>().risk = true;
			}
		}
		// check if the player making their call is marked as With if they win and not the winning bidder
		if(ActivePlayer.GetComponent<PlayerScript_ECalculator>().callCount == highestBidAmount && ActivePlayer != WinningBidder)
		{
			ActivePlayer.GetComponent<PlayerScript_ECalculator>().with = true;
		}
		remainingCalls--;
		nextTurn();
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void BeginPlay() {
		GameObject[] players = GameObject.FindGameObjectsWithTag("ECalc_Player");
		estimating = false;
		playing = true;
		GetComponent<RoundStorage>().rounds[GetComponent<RoundStorage>().currentRound].players[0].call = GameObject.Find(player1Name).GetComponent<PlayerScript_ECalculator>().callCount;
		GetComponent<RoundStorage>().rounds[GetComponent<RoundStorage>().currentRound].players[1].call = GameObject.Find(player2Name).GetComponent<PlayerScript_ECalculator>().callCount;
		GetComponent<RoundStorage>().rounds[GetComponent<RoundStorage>().currentRound].players[2].call = GameObject.Find(player3Name).GetComponent<PlayerScript_ECalculator>().callCount;
		GetComponent<RoundStorage>().rounds[GetComponent<RoundStorage>().currentRound].players[3].call = GameObject.Find(player4Name).GetComponent<PlayerScript_ECalculator>().callCount;
		playerTurn = WinningBidder.GetComponent<PlayerScript_ECalculator>().playerNumber;
	}


}
