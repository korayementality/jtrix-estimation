﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript_ECalculator : MonoBehaviour {

	public GameObject TableManager;
	public string name;
	public int playerNumber;
	public int score;
	public int callCount;
	public int bidCount;
	public int bidColor;
	public int tricks;
	public bool dashCall, passed, risk, doubleRisk, with, win, BidWinner;

	// Use this for initialization
	void Start () {
		TableManager = GameObject.Find("TableManager");
		bidColor = -1;
		StartCoroutine(initializePlayers());
		win = false;
	}
	public void setDashCall(bool isDashcall) {
		TableManager.GetComponent<TableManagerScript>().thereIsADashCaller = isDashcall;
		dashCall = isDashcall;
	}
	public void CalculateScore() {
		int currentRound = TableManager.GetComponent<RoundStorage>().currentRound;
		if(tricks == callCount)
		{
			win = true;
			if(!dashCall)
			{
				score += callCount + 10;
				if(BidWinner)
				{
					score += 20;
				}
				if(risk ^ with)
				{
					score += 20;
				}
				else if(risk && with) 
				{
					score += 30;
				}
				TableManager.GetComponent<RoundStorage>().rounds[currentRound].players[playerNumber-1].score = score;
			}
			else 
			{
				score += 23;
				TableManager.GetComponent<RoundStorage>().rounds[currentRound].players[playerNumber-1].score = score;
			}
		}
		else
		{
			if(!dashCall)
			{
				score -= Mathf.Abs(tricks-callCount);
				if(BidWinner)
				{
					score -= 10;
				}
				if(risk ^ with)
				{
					score -= 10;
				}
				else if(risk && with) 
				{
					score -= 20;
				}
				TableManager.GetComponent<RoundStorage>().rounds[currentRound].players[playerNumber-1].score = score;
			}
			else
			{
				score -= 23;
				TableManager.GetComponent<RoundStorage>().rounds[currentRound].players[playerNumber-1].score = score;
			}
		}
		TableManager.GetComponent<RoundStorage>().rounds[currentRound].players[playerNumber-1].score = score;
	}
	IEnumerator initializePlayers() {
		yield return new WaitForSeconds(0.1f);
		if(playerNumber == 1)
		{
			name = TableManager.GetComponent<TableManagerScript>().player1Name;
			gameObject.name = name;
			TableManager.GetComponent<TableManagerScript>().ActivePlayer = gameObject;
			// GameObject.Find("Canvas/BiddingPanel/Player1Name").GetComponent<Text>().text = name;
			// GameObject.Find("Canvas/DashCallPanel/Player1Name").GetComponent<Text>().text = name;
		}
		if(playerNumber == 2)
		{
			name = TableManager.GetComponent<TableManagerScript>().player2Name;
			gameObject.name = name;
			// GameObject.Find("Canvas/BiddingPanel/Player2Name").GetComponent<Text>().text = name;
			// GameObject.Find("Canvas/DashCallPanel/Player2Name").GetComponent<Text>().text = name;
		}
		if(playerNumber == 3)
		{
			name = TableManager.GetComponent<TableManagerScript>().player3Name;
			gameObject.name = name;
			// GameObject.Find("Canvas/BiddingPanel/Player3Name").GetComponent<Text>().text = name;
			// GameObject.Find("Canvas/DashCallPanel/Player3Name").GetComponent<Text>().text = name;
		}
		if(playerNumber == 4)
		{
			name = TableManager.GetComponent<TableManagerScript>().player4Name;
			gameObject.name = name;
			// GameObject.Find("Canvas/BiddingPanel/Player4Name").GetComponent<Text>().text = name;
			// GameObject.Find("Canvas/DashCallPanel/Player4Name").GetComponent<Text>().text = name;
		}
	}

	// Update is called once per frame
	void Update () {
		BeginOwnTurn();
	}

	void BeginOwnTurn ()
	{
		if(TableManager.GetComponent<TableManagerScript>().newTurn && TableManager.GetComponent<TableManagerScript>().playerTurn == playerNumber && !dashCall && !passed)
		{
			TableManager.GetComponent<TableManagerScript>().ActivePlayer = gameObject;
		}
		else if (TableManager.GetComponent<TableManagerScript>().playerTurn == playerNumber && (dashCall || passed))
		{
			TableManager.GetComponent<TableManagerScript>().nextTurn();
		}
	}
}
