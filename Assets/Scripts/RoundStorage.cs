﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class RoundStorage : MonoBehaviour {
	public int currentRound;
 [System.Serializable]
	public class Round  {
		public List<PlayerInfo> players;
		
	}
	[System.Serializable]
	public class PlayerInfo {
		string name;
		public int score, call, tricks;
		public PlayerInfo(string playerName) {
			name = playerName;
		}
	}
	public Round[] rounds;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void InitializeRoundStorage() {
		foreach(Round round in rounds)
		{
			round.players.Add(new PlayerInfo(GetComponent<TableManagerScript>().player1Name));
			round.players.Add(new PlayerInfo(GetComponent<TableManagerScript>().player2Name));
			round.players.Add(new PlayerInfo(GetComponent<TableManagerScript>().player3Name));
			round.players.Add(new PlayerInfo(GetComponent<TableManagerScript>().player4Name));
		}
	}
	public void CalculateScores() {

	}
	public void EndRound(int player1Tricks, int player2Tricks, int player3Tricks, int player4Tricks) {
		GameObject[] players = GameObject.FindGameObjectsWithTag("ECalc_Player");
		GameObject.Find(GetComponent<TableManagerScript>().player1Name).GetComponent<PlayerScript_ECalculator>().tricks = player1Tricks;
		GameObject.Find(GetComponent<TableManagerScript>().player2Name).GetComponent<PlayerScript_ECalculator>().tricks = player2Tricks;
		GameObject.Find(GetComponent<TableManagerScript>().player3Name).GetComponent<PlayerScript_ECalculator>().tricks = player3Tricks;
		GameObject.Find(GetComponent<TableManagerScript>().player4Name).GetComponent<PlayerScript_ECalculator>().tricks = player4Tricks;
		foreach (var player in players)
		{
			player.GetComponent<PlayerScript_ECalculator>().CalculateScore();
		}
		rounds[currentRound].players[0].tricks = player1Tricks;
		rounds[currentRound].players[1].tricks = player2Tricks;
		rounds[currentRound].players[2].tricks = player3Tricks;
		rounds[currentRound].players[3].tricks = player4Tricks;
		currentRound++;
		GameObject.Find("MenuManager").GetComponent<MenuManagerScript>().SetMenuState(0);
		GetComponent<TableManagerScript>().BeginDashCalls();

	}
	public void SaveRound () {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/tableInfo.dat");
		bf.Serialize(file, rounds);
		file.Close();

	}
	public void LoadRound () {
		if(File.Exists(Application.persistentDataPath + "/tableInfo.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/tableInfo.dat", FileMode.Open);
			rounds = (Round[])bf.Deserialize(file);
			file.Close();
		}
	}
}
