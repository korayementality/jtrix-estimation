﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManagerScript : MonoBehaviour {

	public int menuState;
	public GameObject GameManager;
	public GameObject canvas;
	public GameObject currentPanel;
	// Use this for initialization
	void Start () {
		GameManager = GameObject.Find("GameManager");
		canvas = GameObject.Find("Canvas");
		currentPanel = canvas.transform.GetChild(menuState).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		displayTargetPanel();
	}
	public void TogglePopupPanel(GameObject panel) {
		if(panel.activeInHierarchy)
		{
			panel.SetActive(false);
		}
		else
		{
			panel.SetActive(true);
		}
	}
	void displayTargetPanel() {
		currentPanel = canvas.transform.GetChild(menuState).gameObject;
		if(!currentPanel.activeInHierarchy)
		{
			currentPanel.SetActive(true);
		}
	}
	public void SetMenuState(int targetState) {
		canvas.transform.GetChild(menuState).gameObject.SetActive(false);
		menuState = targetState;
	}
}
